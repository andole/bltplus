
if not MenuHelperHelperBLTPlus then
	log('[BLT+] ERROR: MenuHelperHelperBLTPlus is missing')
	return
end

local thisPath, thisDir, upDir, thisFolder
local function Dirs()
	thisPath = debug.getinfo(2, "S").source:sub(2)
	thisDir = string.match(thisPath, '.*/')
	upDir, thisFolder = thisDir:match('(.*/)([^/]+)/$')
end
Dirs()
Dirs = nil


-- C for 'class'
BLTplusC = BLTplusC or blt_class()

function BLTplusC.Dirs()
	return thisDir, thisFolder, upDir
end


dofile(thisDir..'BLTplus_menu.lua')
dofile(thisDir..'BLTplus_find_mods.lua')
dofile(thisDir..'BLTplus_checks.lua')
dofile(thisDir..'BLTplus_repos_api.lua')
dofile(thisDir..'BLTplus_updates.lua')
dofile(thisDir..'req/BLTplusRepoUpdate.lua')


BLTplusC.loc = {}
function BLTplusC.Loc(id, id2)
	id = id2 or id -- If called with colon BLTplusC:Loc()
	if not id then return 'nil' end
	return BLTplusC.loc[id] or id
end


function BLTplusC.QMenu(this, title, text)
	if not text then
		text = title
		title = this
	end
	QuickMenu:new(title,text,{{ text = BLTplusC.Loc('ok'), is_cancel_button = true }}):Show()
end


function BLTplusC.FixModTxt(path, json_data)
	local file = io.open(path .. 'mod.txt', "w")
	if file then
		json_data['blt_version'] = BLT.version or 2.0
		json_data['updates'] = nil
		-- This BS gsub \/ to / always adds '2' to the end of the string
		local data = json.encode(json_data):gsub([[\/]],[[/]]):gsub('[^}]-$', '')
		file:write(data)
		file:close()
	end
end

-- If notification icon should be displayed
function BLTplusC:ShowNotifications()
	local val = self.menu.hide_notification.value or 1
	if val == 3 then return false end
	val = self.notifications > 0 and 1 or val
	return val == 1
end


function BLTplusC:UpdateNotifications(value, id)
	if id then
		if self.notifications_ids[id] then
			return
		else
			self.notifications_ids[id] = true
		end
	end
	
	self.notifications = self.notifications + value
	
	if managers and managers.menu_component then
		local notifications = managers.menu_component:blt_notifications_gui()
		if notifications then
			notifications:update_outdated_mods_notification(value)
		end
	end
end


function BLTplusC:AddNotification(id, callback, disable_item)
	if disable_item == nil then disable_item = true end
	
	BLTplusC.MHH.SetValue(nil, BLTplusC.menu_names.actions..':'..id, function(item)
		if disable_item then
			item:set_visible(false)
			item:set_enabled(false)
		end
		callback(item)
	end)
end


function BLTplusC:AddRequiredAction(id, clbk)
	self.required_actions = self.required_actions or {}
				
	local not_id = id
	while self.required_actions[not_id] do
		not_id = ' '..not_id
	end
	
	self.required_actions[not_id] = clbk

	self.notifications = self.notifications + 1
end


function BLTplusC:LoadLocFile(val, add_strs)
	val = val or self.menu.language.value or 1			
	dofile(thisDir..'loc/'..(type(val) == 'string' and val or (self.menu.language[val] or 'en'))..'.lua')
	
	if add_strs ~= nil and not add_strs then return end
	
	LocalizationManager:add_localized_strings(BLTplusC.loc_menu)
end