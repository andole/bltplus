--[[

	Provides ultimate hooking functionality, as well as events.
	

	Hooker:Hook(event_id, func, force, prehook)
		Hooks a function to run before or after an event
		returns the original function
		
	event_id  If contains a dot or a colon, treated as a function 'address'
	          If not then hooks to an event with such ID
			  The function must exist, the event does not have to
			  
	func      The function to run when the event or the function is called
	
	force     If true and event_id is a function, but it does not exist, then it will be created
	
	prehook   If true, the function will be called before the original function
	          Hooker:PreHook() exists to omit this


	Function hooks are run on themselves when the original function is called.
	Events must be started with Hooker:Call().
	
	Hooker:Call(event_id, data)
		Starts the event by event_id 
		Results in all of the functions hooked to this event getting called
	
	data      { var1 = value1, var2 = value2, var3 = value3 }
			  'data' is passed as the argument to hooked functions
			  If a hooked function returns a table, it's merged with 'data'
			  Defaults to {}


	Hooker:Args()
		Returns '_args'
		For events, '_args' is their 'data'
		For function hooks, '_args' contains special fields
	
	local var1, var2, var3 = Hooker:Args('var1, var2, var3')
		This is mostly for events
	
	Hooker:Args(nil, new_args)
		Sets 'new_args' to be the new '...' ('self' not included)
		Affects further calls of hooked functions and the original function
		
	Hooker:Args('var1, var2, var3', new_args)
		Sets those fields in '_args' to match them in new_args
		Use to exchange data among hooked functions
		
	
	Special fields of '_args':
	
	self            If a colon was used in the event_id, then this will contain 'self'
	
	_orig_func      The original function
	
	_orig_return    Available for post hooks, contains what the original function returned
	
	_ignore_return  If a function pre-hooked to a function returns something,
	                 Hooker will return that immediately as the result of the original function
                    Set '_ignore_return' to 'true' to avoid that if impossible otherwise
					Resets after every hooked function call
					
	_orig_args      The list of arguments initially passed to the original function
	
	_new_args       Becomes the new '...' for further calls of the original and hooked functions
 ]]


Hooker = {}

local _args = {}
local _current_event_id = ''

local _events = {}
local _prehooks = {}
local _hooks = {}


function Hooker:Args(str, new_args)
	
	if not str and not new_args then
		return _args[_current_event_id]
	end
	
	if not new_args then
		local ret = {}
		for v in str:gmatch('[%a%d_]+') do
			table.insert(ret, _args[_current_event_id][v])
		end
		return unpack(ret)
	end
	
	if not str then
		_args[_current_event_id]._new_args = new_args
		return
	end
	
	for v in str:gmatch('[%a%d_]+') do
		_args[_current_event_id][v] = new_args[v]
	end
end


function Hooker:Hook(event_id, func, force, prehook)

	if type(func) ~= 'function' then
		return false, 'not a function'
	elseif type(event_id) ~= 'string' then
		return false, 'not a string'
	end

	if _events[event_id] then
		table.insert(_events[event_id], func)
	else
		if event_id:match('[%.:]') then
		
			if _prehooks[event_id] then
				if prehook then
					table.insert(_prehooks[event_id], 1, func)
				else
					table.insert(_hooks[event_id], func)
				end
				return true
			end
		
			local res, er = Hooker:HookFunc(event_id, force)
			if not res then
				return false, er
			end
			
			_prehooks[event_id] = {}
			_hooks[event_id] = {}
			
			if prehook then
				table.insert(_prehooks[event_id], 1, func)
			else
				table.insert(_hooks[event_id], func)
			end
			
			if er then return er end
		else
			_events[event_id] = {}
			table.insert(_events[event_id], func)
		end
	end
	
	return true
end

function Hooker:PreHook(event_id, func, force)
	return Hooker:Hook(event_id, func, force, true)
end


function Hooker:Call(id, data)
	if not _events[id] then
		return false, 'no such event '
	end
	
	if data and type(data) ~= 'table' then
		return false, 'data not table'
	end
	
	_current_event_id = id
	_args[id] = data or {}
	
	for k,func in ipairs(_events[id]) do
		local res = func(_args[id])
		
		_current_event_id = id
		
		if type(res)=='table' then
			for k,v in pairs(res) do
				_args[id][k] = v
			end
		end
	end
end

function Hooker:HookFunc(id, force)
	local split = {}
	for v in id:gmatch('[^%.]+') do
		table.insert(split, v)
	end
	
	local tbl
	if #split > 1 then
	
		_G[split[1]] = _G[split[1]] or (force and {} or nil)
		
		tbl = _G[split[1]]
		
		local i
		for i=2,#split-1 do
			if type(tbl) ~= 'table' then
				return false, 'not found '..split[i-1]
			end
			
			if not tbl[split[i]] and force then
				tbl[split[i]] = {}
			end
			
			tbl = tbl[split[i]] or nil
		end
		
		if type(tbl) ~= 'table' then
			return false, 'not found '..split[i]
		end
	else
		tbl = _G
	end
	
	local field, value = split[#split]:match('^[^:]+'), split[#split]:match(':(.+)$')
	
	tbl[field] = tbl[field] or (force and {} or nil)
	
	if not field or not tbl[field] then
		return false, 'not found '..field
	end
	
	local uses_this = false
	if value then
		tbl = tbl[field]
		
		tbl[value] = tbl[value] or (force and function() end or nil)
		
		if not tbl[value] or type(tbl[value]) ~= 'function' then
			return false, 'not found '..value
		end
		
		uses_this = true
	else
		tbl[field] = tbl[field] or (force and function() end or nil)
	
		if type(tbl[field]) ~= 'function' then
			return false, 'target not function '..field
		end
		
		value = field
	end
	
	local origfunc = tbl[value]
	
	tbl[value] = function(this, ...)
		_current_event_id = id
		_args[id] = {}
		
		if uses_this then
			_args[id] = { self = this, _orig_func = origfunc }
		else
			_args[id] = { _orig_func = origfunc }
		end
		
		_args[id]._orig_args = {}
		for i=1,select('#', ...) do
			_args[id]._orig_args[i] = select(i, ...) or false -- Nil values workaround
		end
		
		Hooker:CallFunc(true, id, uses_this and this or nil, ...)
		
		if _args[id]._return then
			_args[id]._orig_return = _args[id]._return
		else
			if _args[id]._new_args then
				_args[id]._orig_return = {origfunc(this, unpack(_args[id]._new_args))}
			else
				_args[id]._orig_return = {origfunc(this, ...)}
			end
			
			_current_event_id = id
		end
		
		Hooker:CallFunc(false, id, uses_this and this or nil, ...)
		
		local ret = _args[id]._return or _args[id]._orig_return or nil
		if ret then
			return unpack(ret)
		end
	end
	
	return true, origfunc
end

function Hooker:CallFunc(prehook, id, this, ...)
	local ret
	local tbl = prehook and _prehooks or _hooks
	
	for k, func in ipairs(tbl[id]) do
		_args[id]._ignore_return = nil
		
		if this then
			if _args[id]._new_args then
				ret = {func(this, unpack(_args[id]._new_args))}
			else
				ret = {func(this, ...)}
			end
		else
			if _args[id]._new_args then
				ret = {func(unpack(_args[id]._new_args))}
			else
				ret = {func(...)}
			end
		end
		
		_current_event_id = id
		
		if ret and #ret > 0 and not _args[id]._ignore_return then
			if prehook then
				_args[id]._return = ret
				return
			else
				_args[id]._return = ret
				_args[id]._orig_return = ret
			end
		end
		ret = nil
	end
end