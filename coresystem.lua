local thisPath, thisDir, upDir, thisFolder
local function Dirs()
	thisPath = debug.getinfo(2, "S").source:sub(2)
	thisDir = string.match(thisPath, '.*/')
	upDir, thisFolder = thisDir:match('(.*/)([^/]+)/$')
end
Dirs()
Dirs = nil


dofile(thisDir..'req/MenuHelperHelper.lua')
dofile(thisDir..'req/Hooker.lua')
dofile(thisDir..'BLT/BLTMod.lua')
dofile(thisDir..'BLTplus.lua')

if not BLTplusC then
	log('[BLT+] ERROR: BLTplusC is missing')
	return
end

BLTplus = BLTplusC:new()

dofile(thisDir..'BLT/BLTUpdate.lua')

local mods_list = BLT:ProcessModsList(BLTplus:FindMods())

dofile(thisDir..'BLT/BLTModManager.lua')

for k,v in pairs(mods_list) do
	table.insert(BLT.Mods.mods, v)
end


-- Do this for the newfound categorized mods
if BLT.Mods._saved_data then
	local saved_data = BLT.Mods._saved_data
	
	if saved_data["mods"] then
		for index, cat in pairs(BLTplus.mods) do
			local id = cat..'/'..index
			if saved_data["mods"][index] or saved_data["mods"][id] then

				local data = saved_data["mods"][index] or saved_data["mods"][id]
				local mod = BLT.Mods:GetMod(id)
				mod:SetEnabled( data["enabled"], true )
				mod:SetSafeModeEnabled( data["safe"] )
				
				local updates = data["updates"]
				if updates and type(updates) == 'table' then
					for update_id, enabled in pairs( updates ) do
						local update = mod:GetUpdate( update_id )
						if update then
							update:SetEnabled( enabled )
						end
					end
				end
			end
		end
	end
end

BLTplus:CheckCrash()

BLTplus:SetupMods()

BLTplus:CheckModsAndNotify()

BLTplus:CreateNotificationsMenu()

-- Preload images
for k,v in pairs(BLT.Mods.mods or {}) do
	v:GetModImage()
end