local REPOS = {
	bitbucket = {
			last_commit = {
				url = 'https://api.bitbucket.org/2.0/repositories/%s/%s/commit/%s',
				fields = { hash = 'hash', message = 'message' }
			},
			compare = {
				url = function(user,project,branch,commit)
						return string.format('https://api.bitbucket.org/2.0/repositories/%s/%s/diff/%s..%s', user,project,branch,commit)
					end,
				fields = nil
			},
			raw = function(user,project,branch,file)
						return string.format('https://bitbucket.org/%s/%s/raw/%s/%s', user,project,branch,file)
					end,
			commits_page = 'https://bitbucket.org/%s/%s/commits/branch/%s'
	},
	github = {
			last_commit = {
				url = 'https://api.github.com/repos/%s/%s/commits/%s',
				fields = { hash = 'sha', msg_field = 'commit', message = 'message' }
			},
			compare = {
				url = 'https://api.github.com/repos/%s/%s/compare/%s...%s',
				fields = { files = 'files', filename = 'filename' },
				
				GetFileNames = function(tbl)
					local res = {}
					if tbl.status then
						if tbl.status == 'removed' then
							res.delete = tbl.filename
						elseif tbl.status == 'renamed' then
							res.delete = tbl.previous_filename
							res.update = tbl.filename
						else
							res.update = tbl.filename
						end
					end
					return res
				end
			},
			raw = 'https://raw.github.com/%s/%s/%s/%s',
			
			commits_page = 'https://github.com/%s/%s/commits/%s'
		},
	gitlab = {
			last_commit = {
				url = 'https://gitlab.com/api/v4/projects/%s%%2F%s/repository/commits/%s',
				fields = { hash = 'id', message = 'message' }
			},
			compare = {
				url = 'https://gitlab.com/api/v4/projects/%s%%2F%s/repository/compare?from=%s&to=%s',
				fields = { files = 'diffs', filename = 'new_path' },
				
				GetFileNames = function(tbl)
					local res = {}
					if tbl.renamed_file then
						res.delete = tbl.old_path
						res.update = tbl.new_path
					elseif tbl.deleted_file then
						res.delete = tbl.old_path
					else
						res.update = tbl.new_path
					end
					return res
				end
			},
			raw = function(user,project,branch,file)
					return string.format('https://gitlab.com/api/v4/projects/%s%%2F%s/repository/files/%s/raw?ref=%s', user,project,file,branch)
			end,
			
			format_raw = function(url, str)
				return url:gsub('%%s', str:gsub('[/\\]','%%%%2F'), 1)
			end,
			
			commits_page = 'https://gitlab.com/%s/%s/commits/%s'
		}
}

BLTplusC.repos_api = {}

function BLTplusC.repos_api.FormatRawFileUrl(url, filename)
	for k,v in pairs(REPOS) do
		if url:match(k) then
			if v.format_raw then
				return v.format_raw(url, filename)
			end
			break
		end
	end
	return url:gsub('%%s', filename, 1)
end

function BLTplusC.repos_api.GetRawFileUrl(host)
	if not REPOS[host.platform] then
		return
	end
	
	local raw = REPOS[host.platform].raw
	if type(raw) == 'function' then
		raw = raw(host.user, host.project, host.branch or 'master', '%s')
	else
		raw = string.format(raw, host.user, host.project, host.branch or 'master', '%s')
	end
	
	return raw
end


function BLTplusC.repos_api.GetCommitsUrl(host)
	if not REPOS[host.platform] then
		return
	end
	
	local url = REPOS[host.platform].commits_page
	if url then
		return string.format(url, host.user, host.project, host.branch or 'master')
	end
end

-- Get last commit hash and message
function BLTplusC.repos_api.GetLastCommit(host, clbk)

	if not REPOS[host.platform] then
		clbk(false, 'Unknown repository platform')
		return
	end
	
	local lc = REPOS[host.platform].last_commit
	dohttpreq(string.format(lc.url, host.user, host.project, host.branch or 'master'), function(body, http_id)
		if body:is_nil_or_empty() then
			clbk(false, 'Empty data received')
			return
		end
		
        local data = json.decode(body)
		
		if not data or not data[lc.fields.hash] then
			clbk(false, 'Incorrect received data format')
			return
		end
		
		local message = data[lc.fields.msg_field or lc.fields.message]
        message = lc.fields.msg_field and message[lc.fields.message] or message
		
		clbk(true, data[lc.fields.hash], message)
	end)
end

-- Get 'diff' between saved commit hash and head of the needed branch
-- then return the list of changed files
function BLTplusC.repos_api.GetFilesToUpdate(host, hash, clbk)

	if not REPOS[host.platform] then
		clbk(false, 'Unknown repository platform')
		return
	end
	
	local comp = REPOS[host.platform].compare
	local url = comp.url
	
	if type(url) == 'function' then
		url = url(host.user, host.project, host.branch or 'master', hash)
	else
		url = string.format(url, host.user, host.project, hash, host.branch or 'master')
	end
	
	dohttpreq(url, function(body, http_id)
		if body:is_nil_or_empty() then
			clbk(false, 'Empty data received')
			return
		end
		
		local filenames = {}
		local removed = {}
		if comp.fields then
			local data = json.decode(body)
			
			if not data or not data[comp.fields.files] then
				clbk(false, 'Incorrect received data format')
				return
			end
			
			for k,v in pairs(data[comp.fields.files]) do
				if comp.GetFileNames then
					local files = comp.GetFileNames(v)
					table.insert(removed, files.delete)
					table.insert(filenames, files.update)
				elseif v[comp.fields.filename] then
					table.insert(filenames, v[comp.fields.filename])
				end
			end
		else
			-- Raw patch
			local have = {}
			have['dev/null'] = true
			for old, new in body:gmatch('%-%-%- a?/([^\n]+)\n%+%+%+ b?/([^\n]+)\n@@') do
				if new == 'dev/null' and not have[old] then
					have[old] = true
					table.insert(removed, old)
				elseif not have[new] then
					have[new] = true
					table.insert(filenames, new)
				end
			end
		end
		
		if host.filename_prefix then
			for k,v in ipairs(filenames) do
				filenames[k] = v:gsub('^'..host.filename_prefix,'')
			end
			for k,v in ipairs(removed) do
				removed[k] = v:gsub('^'..host.filename_prefix,'')
			end
		end
		
		clbk(true, filenames, removed)
	end)
end