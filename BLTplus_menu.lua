local DEBUG = false -- Allows to browse and change saved data in the menu

local thisDir, thisFolder, upDir = BLTplusC.Dirs()

-- Save the function because it will be removed once menus are created
BLTplusC.MHH = { SetValue = MenuHelperHelperBLTPlus.SetValue }

BLTplusC.menu_names = { settings = 'base_plus_menu', actions = 'base_plus_menu_actions' }


function BLTplusC:init()
	self.menu = {
		notifs_thru_updates = true, -- Allow notifications coming from updates data
		hide_notification = { 'show', 'show_notifs', 'hide', value = 1 },
		language = {},
		gap1 = '_ 30',
		gap2 = '_ 30',
		mods_menu = { 
			images_row = { 'two', 'three', 'four', 'five', 'six', 'seven', value = 4}, 
			change_menu = true 
		},
		should_load = {}, -- should_load[ mod_category ]
		checked_mods = {}, -- Mods checked for compatibility
		failed_update = {}, -- Stores 'streaks' of update failures for each mod
		repos_commits = {}, -- Stores last commit hash for mods that watch repositories
		notifications = {}, -- Stores last notifications displayed by mods
		
		Save = function() self:Save() end
	}
		
	self.mods = {} -- Stores IDs of categorized mods as keys and their categories as values
		
	self.notifications = 0
	self.notifications_ids = {}

	-- self.repos_updates = {} -- Repositories to check for updates
	
	-- Detect localization files and set up menu item table for language selection
	for k,v in ipairs(file.GetFiles(thisDir..'loc/') or {}) do
		local filelang = v:match('^[^%.]+')
		if filelang == 'en' then
			table.insert(self.menu.language, 1, filelang)
		else
			table.insert(self.menu.language, filelang)
		end
	end
end


function BLTplusC:Save()
	local clbk_id = 'MHH_save_' .. BLTplusC.menu_names.settings
	if MenuCallbackHandler and MenuCallbackHandler[clbk_id] then
		MenuCallbackHandler[clbk_id](nil, nil, self.menu)
	end
end


function BLTplusC:SettingsMenuCallback(settings, menu_id)

	-- If language has changed then load up the new loc file and reenter the menu
	if self.menu.language.value ~= settings.language.value then
		self:LoadLocFile(settings.language.value)

		managers.menu:back()
		managers.menu:open_node(BLTplusC.menu_names.settings)
	end
	
	-- Replace the settings table with the new table from the menu
	self.menu = settings
	
	self:UpdateNotifications(0)
	
	if self.menu.mods_menu.images_row.value then
		BLTModItem.layout.x = self.menu.mods_menu.images_row.value + 1
	end
end


function BLTplusC:CreateSettingsMenu()

	-- Menu tweaks
	MenuHelperHelperBLTPlus._tweaks = {
		hide_notification = { priority = 10 },
		images_preload = { priority = 9 },
		notifs_thru_updates = { priority = 8 },
		mods_menu = { priority = 1 },
		gap1 = { priority = 7 },
		gap2 = { priority = 11 },
		language = { priority = 12 },
		
		checked_mods = { ignore = not DEBUG, localized = false },
		failed_update = { ignore = not DEBUG, localized = false },
		repos_commits = { ignore = not DEBUG, localized = false },
		notifications = { ignore = not DEBUG, localized = false },
		Save = { ignore = not DEBUG, localized = false },
		
		default = { 
			save_only_changed = true,
			instant_callback = true,
			priority = 0,
			localized = not DEBUG
		}
	}

	-- Do not localize mod folder names in the menu
	for k,v in pairs(self.menu.should_load) do
		MenuHelperHelperBLTPlus._tweaks[k] = { localized = false }
	end
		
	-- Create the menu
	MenuHelperHelperBLTPlus:CreateMenu(self.menu, BLTplusC.menu_names.settings, 'base+.txt', function(settings, menu_id)
		self:SettingsMenuCallback(settings, menu_id)
	end)
	
	-- Make language value a string
	Hooker:Hook('MenuManager_init', function() -- This hooks to 'MenuManager_init' event (we're in coresystem)
		-- This hooks to the menu callback function
		Hooker:PreHook('MenuCallbackHandler.MHH_save_'..BLTplusC.menu_names.settings, function(arg1, tbl)
			tbl = tbl or self.menu
			if tbl then
				tbl.language.value = tbl.language[tbl.language.value] or tbl.language.value
			end
			-- This replaces further function arguments
			Hooker:Args(nil, { arg1 or false, tbl })
		end)
	end)
	
	local old_lang_value = self.menu.language.value
	if type(old_lang_value) == 'string' then
		local found = false
		for k,v in ipairs(self.menu.language) do
			if v == old_lang_value then
				old_lang_value = k
				found = true
				break
			end
		end
		if not found then old_lang_value = 1 end
	end
	
	self:LoadLocFile(nil, false)
	
	-- We're in coresystem, so add hooks for later
	Hooker:Hook('LocalizationManager_init', function()
		local lang = BLT.Localization:get_language()
		lang = lang.language or 'en'
		
		if file.DirectoryExists('PD2TL/') then
			lang = 'th'
		end
		
		for k,v in pairs(BLT.Mods:Mods()) do
			if v:GetName():match('^Spolszczenie') then
				lang = 'pl'
				break
			end
		end
		
		local loctable = {}
		loctable[BLTplusC.menu_names.settings..'_language'] = 'Language'
		loctable[BLTplusC.menu_names.settings..'_language_desc'] = ' '
		
		for k,v in ipairs(self.menu.language) do
			loctable[BLTplusC.menu_names.settings..'_language_'..v] = v:upper()
			
			if v == lang then
			  if old_lang_value then
			  
				-- See if a new language is available which would be a better fit
			    local last_suggested = self.menu.language.last_suggested or 'en'
			    if old_lang_value ~= k and last_suggested ~= lang then
			      local notif_id = '['..lang..']'
				  
				  Hooker:Hook('MenuManager_init', function()
					  self:AddNotification(notif_id, function(item)
						QuickMenu:new(
						  notif_id, 
						  string.format(BLTplusC.Loc('lang_available'),lang:upper()),
						  {
							{ text = string.format(BLTplusC.Loc('use_lang'), notif_id),
								callback = function()
								  self.menu.language.value = k
						
								  BLTplusC.MHH.SetValue(nil, BLTplusC.menu_names.settings..':language', k)
								  
								  self:LoadLocFile()
								  self.menu.Save()
							  end },
											
							{ text = string.format(BLTplusC.Loc('stay_with_lang'),' ['..self.menu.language[old_lang_value]..']'),
								callback = function()
								  self.menu.language.last_suggested = lang
								  self.menu.Save()
							  end }
						  },
						true)
						self:UpdateNotifications(-1, notif_id..'_lang')
					  end, false)
					  self:UpdateNotifications(1)
				  end)
			    end
			  else
			    -- If first launch
				self.menu.language.value = k
				
				self:LoadLocFile(k)
				
				-- This will set the multiple choice item to the correct value
				Hooker:Hook('MenuManager_init', function()
					BLTplusC.MHH.SetValue(nil, BLTplusC.menu_names.settings..':language', k)
				end)
			  end
			end
		end
		
		LocalizationManager:add_localized_strings(loctable)
		
		local val = self.menu.language.value or 1
		self.menu.language.value = val
		
		LocalizationManager:add_localized_strings(BLTplusC.loc_menu)
	end)
end


function BLTplusC:CreateNotificationsMenu()

	MenuHelperHelperBLTPlus._tweaks['default'] = { localized = false }
	
	MenuHelperHelperBLTPlus:CreateMenu(self.required_actions or {}, BLTplusC.menu_names.actions, nil, function(settings)
		self:UpdateNotifications(0)
	end)

	self.required_actions = nil
end