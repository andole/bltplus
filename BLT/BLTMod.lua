
if BLTSuperMod then

Hooker:Hook('BLTMod:init', function(self, ident, data)
	if data and BLTplus then
		-- Add updates from updates+; if there's no BLT+ then they won't affect anything
		for i, update_data in ipairs(data["updates+"] or {} ) do
			BLTplus:AddUpdate(self, update_data, true)
		end
	end
end)

end --if BLTSuperMod


function BLTMod:SetCategory(cat)
	self.category = cat
end

function BLTMod:GetCategory()
	return self.category
end

-- Name of the folder with the mod
function BLTMod:SetPureId(id)
	self.pure_id = id
end

function BLTMod:GetPureId()
	return self.pure_id or self.id
end