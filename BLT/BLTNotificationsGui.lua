if not BLTplus then return end

-- Used to prevent a bug where if user clicks the icon several times
--  it opens the same menu several times, so user has to click 'back' the same number of times
local allowed_to_click = true

local col = Color(11/255,83/255,69/255)
local col_bright = Color(220/255,118/255,51/255)

Hooker:Hook('BLTNotificationsGui:_setup', function(self)

	self._outdated_panel = self._panel:panel({
		name = "outdated",
		w = 48,
		h = 48,
		x = self._downloads_panel:x(),
		y = self._downloads_panel:y() + self._downloads_panel:h(),
		layer = 100
	})

	local texture, rect = tweak_data.hud_icons:get_icon_data( "csb_throwables" )
	self._outdated_panel:bitmap({
		name = 'bmap',
		texture = texture,
		texture_rect = rect,
		w = self._outdated_panel:w(),
		h = self._outdated_panel:h(),
		color = BLTplus.notifications > 0 and col_bright or col
	})
	
	self._outdated_panel:rect({
		name = 'rect',
		x = 38/2.5-2,
		y = 28/2.5,
		w = 54/2.5,
		h = 72/2.5-2,
		color = BLTplus.notifications > 0 and col_bright or col
	})
	
	self._outdated_count = self._outdated_panel:text({
		font_size = 20,
		font = tweak_data.menu.pd2_medium_font,
		layer = 10,
		blend_mode = "add",
		color = tweak_data.screen_colors.title,
		text = BLTplus.notifications > 0 and tostring(BLTplus.notifications) or 'BLT+',
		align = "center",
		vertical = "center",
	})
	
	if BLTplus.notifications < 1 then
		self._outdated_panel:set_visible(BLTplus:ShowNotifications())
	end
end)

function BLTNotificationsGui:update_outdated_mods_notification(value)
	self._outdated_panel:set_visible(BLTplus:ShowNotifications())
	
	self._outdated_count:set_text(BLTplus.notifications > 0 and tostring(BLTplus.notifications) or 'BLT+')
	
	local newcolor = BLTplus.notifications > 0 and col_bright or col
	self._outdated_panel:child('rect'):set_color(newcolor)
	self._outdated_panel:child('bmap'):set_color(newcolor)
	
	allowed_to_click = true
end

Hooker:PreHook('BLTNotificationsGui:mouse_pressed', function(self, button, x, y)

	if alive(self._outdated_panel)
		and self._outdated_panel:visible()
		and self._outdated_panel:inside( x, y )
		and allowed_to_click
	then
		if BLTplus.notifications > 0 then
			managers.menu:open_node(BLTplus.menu_names.actions)
		else
			managers.menu:open_node(BLTplus.menu_names.settings)
		end
		allowed_to_click = false
		return true
	end
end)


Hooker:Hook('BLTNotificationsGui.mouse_pressed', function()
	local res = Hooker:Args('_orig_return')[1]
	
	if res then
		allowed_to_click = true
	end
	
	return res
end)

-- Solely to prevent notification box outline from appearing
Hooker:Hook('BLTNotificationsGui:mouse_moved', function(self, o, x, y)
	if alive(self._outdated_panel)
		and self._outdated_panel:visible()
		and self._outdated_panel:inside( x, y )
	then
		if self._content_outline then
			self._content_outline:set_visible(false)
		end
		return true, 'link'
	end
end)