Hooker:PreHook('BLTDownloadManager:start_download', function(self, update)
	if update.BLTplusRepoUpdate then
		local download = {
			update = update,
			http_id = update.id,
			state = "waiting"
		}
		table.insert( self._downloads, download )
		
		update:Update(nil, callback(self, self, "clbk_download_progress"), callback(self, self, "bltp_clbk_repo_download_finished"))
		
		return 0
	end
end)


function BLTDownloadManager:bltp_clbk_repo_download_finished(id, success, error_text)
	local download = self:get_download_from_http_id(id)
	if not download then return end
	
	if not success then
		log('[Downloads+] Repo update failed: '..id..', '..(error_text or '(unknown reason)'))
		download.state = "failed"
		return
	end
	
	download.state = "complete"
end


if BLTSuperMod then

Hooker:PreHook('BLTDownloadManager:clbk_download_finished', function(self, data, http_id)
	local download = self:get_download_from_http_id(http_id)
	if download.update.no_hash then
		self:bltp_clbk_dwnld_fin_no_ver(data, http_id)
		return 0
	end
end)

-- Same as the original, but skips the hash verification part
function BLTDownloadManager:bltp_clbk_dwnld_fin_no_ver(data, http_id)
	local download = self:get_download_from_http_id(http_id)
	log(string.format("[Downloads+] Finished download of %s (%s)", download.update:GetName(), download.update:GetParentMod():GetName()))
	self._coroutine_ws = self._coroutine_ws or managers.gui_data:create_fullscreen_workspace()
	download.coroutine = self._coroutine_ws:panel():panel({})
	local save = function()
		local wait = function( x )
			for i = 1, (x or 5) do
				coroutine.yield()
			end
		end
		local install_dir = download.update:GetInstallDirectory()
		local temp_dir = Application:nice_path( install_dir .. "_temp" )
		if install_dir == BLTModManager.Constants:ModsDirectory() then
			temp_dir = Application:nice_path( BLTModManager.Constants:DownloadsDirectory() .. "_temp" )
		end
		local file_path = Application:nice_path( BLTModManager.Constants:DownloadsDirectory() .. tostring(download.update:GetId()) .. ".zip" )
		local temp_install_dir = Application:nice_path( temp_dir .. "/" .. download.update:GetInstallFolder() )
		local install_path = Application:nice_path( download.update:GetInstallDirectory() .. download.update:GetInstallFolder() )
		local extract_path = Application:nice_path( temp_install_dir .. "/" .. download.update:GetInstallFolder() )
		local cleanup = function()
			SystemFS:delete_file( temp_install_dir )
		end
		wait()
		SystemFS:make_dir( temp_dir )
		SystemFS:delete_file( file_path )
		cleanup()
		log("[Downloads+] Saving to downloads...")
		download.state = "saving"
		wait()
		local f = io.open( file_path, "wb+" )
		if f then
			f:write( data )
			f:close()
		end
		log("[Downloads+] Extracting...")
		download.state = "extracting"
		wait()
		unzip(file_path, temp_install_dir)
		wait()
		log("[Downloads+] Removing old installation...")
		wait()
		
		local old_install_path = install_path .. '_old'
		if file.MoveDirectory( install_path, old_install_path ) then
			SystemFS:delete_file( old_install_path )
		else
			SystemFS:delete_file( install_path )
		end
		
		local move_success = file.MoveDirectory(extract_path, install_path)
		if not move_success then
			if jit and jit.os and not jit.os:lower():match('linux') then
				log("[Downloads+] BLT failed to move the folder. Moving with Windows means...")
				os.execute('move "'..extract_path..'" "'..install_path..'"')
			else
				log("[Downloads+] Failed to move installation directory!")
				download.state = "failed"
				cleanup()
				return
			end
		end
		
		log("[Downloads+] Complete!")
		download.state = "complete"
		cleanup()
	end
	download.coroutine:animate( save )
end

end --if BLTSuperMod