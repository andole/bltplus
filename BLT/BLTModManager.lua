if not BLTplus then return end

local get_mod_orig = Hooker:Hook('BLTModManager:GetMod', function(self, id, ...)
	local origfunc, orig_return = Hooker:Args('_orig_func, _orig_return')
	
	if not orig_return[1] and BLTplus.mods[id] then
		return origfunc(self, BLTplus.mods[id].."/"..id, ...)
	else
		return orig_return[1]
	end
end)

function BLTModManager:GetModOriginal(id, ...)
	return get_mod_orig(self, id, ...)
end

Hooker:PreHook('BLTModManager:_RunAutoCheckForUpdates', function(self)
	if not MenuCallbackHandler:is_online() then
		self._has_checked_for_updates = true
		
		local icon, rect = tweak_data.hud_icons:get_icon_data("csb_pagers")
		BLT.Notifications:add_notification( {
			title = BLTplusC.Loc('offline'),
			text = BLTplusC.Loc('updates_not_checked'),
			icon = icon,
			icon_texture_rect = rect,
			color = Color.white,
			priority = 1000,
		} )
		
		return 0
	end
end)