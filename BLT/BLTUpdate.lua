if not BLTplus then return end

if BLTSuperMod then

Hooker:Hook('BLTUpdate:init', function(self, ident, data)
	self.no_hash = data["no_hash"] or false
end)


Hooker:PreHook('BLTUpdate:clbk_got_update_data', function(self, clbk, json_data, http_id)
	if not self.no_hash then
		return
	end
	
	if json_data:is_nil_or_empty() then
		log("[Error+] Could not connect to the download server!")
		self._error = "Could not connect to the download server."
		return self:_run_update_callback( clbk, false, self._error )
	end

	-- We're done checking for updates
	self._requesting_updates = false

	local server_data = json.decode( json_data )
	if server_data then

		for _, data in pairs(server_data) do
			log(string.format("[Updates+] Received update data for '%s'", data.ident))
			if data.ident == self:GetId() then
				self._update_data = data
				self._server_hash = data.hash
				
				if data.version then
					return self:_run_update_callback(clbk, tostring(data.version) ~= self.parent_mod:GetVersion(), nil)
				end
			end
		end
		
	end

	self._error = "No valid mod ID was returned by the server."
	log("[Updates+] Invalid or corrupt update data for mod " .. self:GetId())
	return self:_run_update_callback( clbk, false, self._error )
end)

end --if BLTSuperMod


Hooker:PreHook('BLTUpdate:clbk_got_update_data', function(self, clbk, json_data)
	BLTplus:CheckUpdatesInfo(self, json_data)
end)

Hooker:Hook('BLTUpdate:clbk_got_update_data', function(self)
	BLTplus:CheckUpdatesError(self)
end)