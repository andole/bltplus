BLTplusC = BLTplusC or {}

BLTplusC.loc = {
	no = 'No',
	ok = 'OK',

	two_mods = 'Ci sono due mod con questo nome:',
	first_loaded = 'La prima è stata caricata.',
	
	older_blt_loaded = 'Questa mod è per una vecchia versione di BLT ma è caricata.\n\nFunziona correttamente?',
	older_blt_not_loaded = 'Questa mod è per una vecchia versione di BLT e non è caricata.\n\nVorresti abilitarla?',
	yes_upgrade = 'Si, falla funzionare con il nuovo BLT',
	
	check_compatibility = 'Controlla compatibilità',
	mods_not_checked = 'Non è stata controllata la compatibilità di queste mod con BLT+:',
	check_now = 'Controlla ora',
	check_later = 'Più tardi',
	
	offline = 'Offline :(',
	updates_not_checked = 'Impossibile controllare gli aggiornamenti.',
	
	mod_fails_updates = 'La mod chiamata \'%s\' non riesce ad aggiornarsi correttamente di recente.\n\nVorresti disattivare gli aggornamenti per questa mod?',
	yes_disable_updates = 'Si, disabilita gli aggiornamenti',
	
	mod_updated_repo = 'Questa mod è stata aggiornata nel suo sito: \'%s\' :', -- %s is github, for example
	no_message = '(nessun messaggio disponibile)',
	disable_forawhile = 'Disabilita per un po di tempo',
	
	all_mods_work = 'Queste mod dovrebbero funzionare correttamente.',
	these_not_work = 'Queste mod potrebbero non funzionare correttamente da una cartella che non è \'mods\':',
	saved_to_log = '(è stato salvato nel Log)',
	mods_check = 'Controlla mod', -- the title of the quickmenu with check result
	
	lang_available = 'Questa lingua è disponibile per BLT+: %s',
	use_lang = 'Usa: %s', -- Use language
	stay_with_lang = 'Continua ad usare: %s', -- Stay with language
	
	not_found_update = 'Non è stato possibile trovare la mod per cui è stato eseguito un aggiornamento: %s',
	
	crashlog = 'Segnala crash',
	copy_crashlog = 'Salva la segnalazione del crash sul desktop'
}


BLTplusC.loc_menu = {

    base_plus_menu = 'Impostazioni BLT+',
    base_plus_menu_desc = 'Impostazioni di BLT+',

    base_plus_menu_images_preload = 'Carica prima le immagini',
    base_plus_menu_images_preload_desc = 'Carica le immagini una volta avviato il gioco',

    base_plus_menu_notifs_thru_updates = 'Notifiche degli aggiornamenti',
    base_plus_menu_notifs_thru_updates_desc = 'Se vuoi ricevere notifiche dalle info degli aggiornamenti scaricati',

    base_plus_menu_mods_menu = 'Menu delle Mod',
    base_plus_menu_mods_menu_desc = 'Menu delle impostazioni di BLT mod manager',

    base_plus_menu_mods_menu_change_menu = 'Cambia menu',
    base_plus_menu_mods_menu_change_menu_desc = 'Cambia ordine delle mod',

    base_plus_menu_mods_menu_images_row = 'Mod di fila',
    base_plus_menu_mods_menu_images_row_desc = 'Quante mod possono essere mostrate su una colonna',
    base_plus_menu_mods_menu_images_row_two = '2',
    base_plus_menu_mods_menu_images_row_three = '3',
    base_plus_menu_mods_menu_images_row_four = '4',
    base_plus_menu_mods_menu_images_row_five = '5',
    base_plus_menu_mods_menu_images_row_six = '6',
    base_plus_menu_mods_menu_images_row_seven = '7',

    base_plus_menu_should_load = 'Gestisci categorie',
    base_plus_menu_should_load_desc = 'Disabilita e abilita categorie mod',
	
    base_plus_menu_actions = 'Notifiche BLT+',
    base_plus_menu_actions_desc = 'Guarda quali azioni sono richieste',
	
    base_plus_menu_hide_notification = 'Mostra icone di BLT+',
    base_plus_menu_hide_notification_desc = 'Quando le icone delle notifiche dovrebbero essere mostrate',
    base_plus_menu_hide_notification_show = 'Mostra',
    base_plus_menu_hide_notification_show_notifs = 'Se ci sono notifiche',
    base_plus_menu_hide_notification_hide = 'Nascondi',
	
    base_plus_menu_check_mods = 'Controlla compatibilità (attendi)',
    base_plus_menu_check_mods_desc = 'Controlla se le mod sono in grado di lavorare dalla categoria delle cartelle',
	
    base_plus_menu_actions_bltp_check_mods = 'Controlla compatibilità',
    base_plus_menu_actions_bltp_check_mods_desc = 'Controlla se le mod sono in grado di lavorare dalla categoria delle cartelle'

}