BLTplusC = BLTplusC or {}

BLTplusC.loc = {
	no = 'Nie',
	ok = 'OK',

	two_mods = 'Znaleziono dwa mody z ta sama nazwa folderu:',
	first_loaded = 'Pierwszy zostal zaladowany.',
	
	older_blt_loaded = 'Ten mod jest przeznaczony dla starszej wersji BLT, ale jest zaladowany.\n\nCzy dziala prawidlowo?',
	older_blt_not_loaded = 'Ten mod jest przeznaczony dla starszej wersji BLT i nie zostal zaladowany.\n\nCzy chcesz zaladowac?',
	yes_upgrade = 'Tak, spraw, by dzialal z nowym BLT',
	
	check_compatibility = 'Sprawdz kompatybilnosc',
	mods_not_checked = 'Te modyfikacje nie zostaly sprawdzone ze zgodnoscia z BLT+:',
	check_now = 'Sprawdz',
	check_later = 'Pozniej',
	
	offline = 'Niedostepny :(',
	updates_not_checked = 'Nie mozna sprawdzic aktualizacji.',
	
	mod_fails_updates = 'Mod o nazwie \'%s\' zostal ostatnio nieprawidlowo zaktualizowany.\n\nCzy chcesz zablokowac dla niego aktualizacje?',
	yes_disable_updates = 'Tak, zablokuj aktualizacje',
	
	mod_updated_repo = 'Ten mod zostal zaktualizowany w \'%s\' przechowalni:',
	no_message = '(Brak dostepnej wiadomosci)',
	disable_forawhile = 'Zablokuj to na jakis czas',
	
	all_mods_work = 'Te mody powinny dzialac prawidlowo.',
	these_not_work = 'Te mody moga dzialac nieprawidlowo z innego folderu niz \'mods\':',
	saved_to_log = '(Zostalo zapisane do dziennika)',
	mods_check = 'Sprawdzanie modow',
	
	lang_available = 'Ten jezyk jest dostepny dla BLT+: %s',
	use_lang = 'Uzyj %s',
	stay_with_lang = 'Zostan z %s',
	
	not_found_update = 'Nie mozna znalezc tej modyfikacji, aby dodac do niej aktualizacje: %s',
	
	crashlog = 'Raport awarii',
	copy_crashlog = 'Zapisz raport awarii na pulpicie'
}


BLTplusC.loc_menu = {

    base_plus_menu = 'Ustawienia BLT+',
    base_plus_menu_desc = 'Ustawienia BLT+',

    base_plus_menu_images_preload = 'Wczytaj obrazy',
    base_plus_menu_images_preload_desc = 'Zaladuj obrazy modyfikacji na poczatku gry',

    base_plus_menu_notifs_thru_updates = 'Powiadomienia z aktualizacji',
    base_plus_menu_notifs_thru_updates_desc = 'Jezeli chcesz otrzymywac powiadomienia z pobranych informacji o aktualizacjach',

    base_plus_menu_mods_menu = 'Menu modow',
    base_plus_menu_mods_menu_desc = 'Ustawienia menu menedzera modow BLT',

    base_plus_menu_mods_menu_change_menu = 'Zmien menu',
    base_plus_menu_mods_menu_change_menu_desc = 'Zmien sposob sortowania modow',

    base_plus_menu_mods_menu_images_row = 'Modyfikacje w rzedzie',
    base_plus_menu_mods_menu_images_row_desc = 'Jak duzo modow wyswietlic w jednym rzedzie',
    base_plus_menu_mods_menu_images_row_two = '2',
    base_plus_menu_mods_menu_images_row_three = '3',
    base_plus_menu_mods_menu_images_row_four = '4',
    base_plus_menu_mods_menu_images_row_five = '5',
    base_plus_menu_mods_menu_images_row_six = '6',
    base_plus_menu_mods_menu_images_row_seven = '7',

    base_plus_menu_should_load = 'Zarzadzaj kategoriami',
    base_plus_menu_should_load_desc = 'Zablokuj i odblokuj kategorie modyfikacji',
	
    base_plus_menu_actions = 'Powiadomienia BLT+',
    base_plus_menu_actions_desc = 'Zobacz, jakie dzialania sa wymagane',
	
    base_plus_menu_hide_notification = 'Pokaz ikone BLT+',
    base_plus_menu_hide_notification_desc = 'Kiedy ikona powiadomienia powinna zostac wyswietlona',
    base_plus_menu_hide_notification_show = 'Pokaz',
    base_plus_menu_hide_notification_show_notifs = 'Tylko Powiadomienia',
    base_plus_menu_hide_notification_hide = 'Ukryj',
	
    base_plus_menu_check_mods = 'Check compatibility (czekaj na)',
    base_plus_menu_check_mods_desc = 'Sprawdz mody, aby mogly pracowac z folderami kategorii',
	
    base_plus_menu_actions_bltp_check_mods = 'Sprawdz kompatybilnosc',
    base_plus_menu_actions_bltp_check_mods_desc = 'Sprawdz mody, aby mogly pracowac z folderami kategorii'

}