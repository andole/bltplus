BLTplusC = BLTplusC or {}

BLTplusC.loc = {
	no = 'Nein',
	ok = 'OK',

	two_mods = 'Es gibt zwei Mods mit diesem Namen:',
	first_loaded = 'Der Erste wurde geladen.',
	
	older_blt_loaded = 'Diese Mod ist für eine ältere Version von BLT, aber wird trozdem geladen.\n\nFunktioniert er korrekt?',
	older_blt_not_loaded = 'Diese Mod ist für eine ältere Version von BLT und wird nicht geladen.\n\nMöchtest du ihn aktivieren?',
	yes_upgrade = 'Ja, mach es kompatibel mit neuem BLT',
	
	check_compatibility = 'Überprüfe Kompatibilität',
	mods_not_checked = 'Diese Mods wurden nicht auf Kompatibilität mit BLT+ geprüft:',
	check_now = 'Jetzt überprüfen',
	check_later = 'Später',
	
	offline = 'Offline :(',
	updates_not_checked = 'Nicht möglich auf Aktualisierungen zu prüfen.',
	
	mod_fails_updates = 'Die Mod \'%s\' ist kürzlich beim Aktualisieren gescheitert.\n\nMöchtest du Aktualisierungen für die Mod deaktivieren?',
	yes_disable_updates = 'Ja, deaktiviere die Aktualisierung',
	
	mod_updated_repo = 'Diese Mod wurde in ihrem \'%s\' Repository aktualisiert:', -- %s is github, for example
	no_message = '(Keine Nachricht verfügbar)',
	disable_forawhile = 'Deaktiviere es für eine Weile',
	
	all_mods_work = 'Diese Mods sollten korrekt funktionieren.',
	these_not_work = 'Diese Mods könnten aus einem anderen Ordner als \'mods\' nicht korrekt funktionieren:',
	saved_to_log = '(Es wurde in den Log geschrieben)',
	mods_check = 'Mods überprüfung', -- the title of the quickmenu with check result
	
	lang_available = 'Diese Sprache ist für BLT+ verfügbar: %s',
	use_lang = 'Benutze %s', -- Use language
	stay_with_lang = 'Bleib bei %s', -- Stay with language
	
	not_found_update = 'Konnte diese Mod nicht finden um Aktualisierungen hinzuzufügen: %s',
	
	crashlog = 'Absturzreport',
	copy_crashlog = 'Speichere Absturzreport auf Desktop'
}


BLTplusC.loc_menu = {

    base_plus_menu = 'BLT+ Einstellungen',
    base_plus_menu_desc = 'BLT+ Einstellungen',

    base_plus_menu_images_preload = 'Bilder vorladen',
    base_plus_menu_images_preload_desc = 'Lade Modbilder beim Spielstart',

    base_plus_menu_notifs_thru_updates = 'Benachrichtigunen von Aktualisierungen',
    base_plus_menu_notifs_thru_updates_desc = 'Wenn du Benachrichtigunen von heruntergeladenen Aktualisierungsinformationen erhalten möchtest',

    base_plus_menu_mods_menu = 'Mods Menu',
    base_plus_menu_mods_menu_desc = 'BLT Mod Manager Menu Einstellungen',

    base_plus_menu_mods_menu_change_menu = 'Ändere Menu',
    base_plus_menu_mods_menu_change_menu_desc = 'Ändere wie Mods sortiert werden',

    base_plus_menu_mods_menu_images_row = 'Mods in Reihe',
    base_plus_menu_mods_menu_images_row_desc = 'Wie viele Mods sollen in einer Reihe angezeigt werden',
    base_plus_menu_mods_menu_images_row_two = '2',
    base_plus_menu_mods_menu_images_row_three = '3',
    base_plus_menu_mods_menu_images_row_four = '4',
    base_plus_menu_mods_menu_images_row_five = '5',
    base_plus_menu_mods_menu_images_row_six = '6',
    base_plus_menu_mods_menu_images_row_seven = '7',

    base_plus_menu_should_load = 'Verwalte Kategorien',
    base_plus_menu_should_load_desc = 'Aktiviere und deaktiviere Modkategorien',
	
    base_plus_menu_actions = 'BLT+ Benachrichtigunen',
    base_plus_menu_actions_desc = 'Sieh was du tun musst',
	
    base_plus_menu_hide_notification = 'Zeige BLT+ Symbol',
    base_plus_menu_hide_notification_desc = 'Wann das Benachrichtigunssymbol angezeigt werden soll',
    base_plus_menu_hide_notification_show = 'Zeigen',
    base_plus_menu_hide_notification_show_notifs = 'Bei Benachrichtigunen',
    base_plus_menu_hide_notification_hide = 'Verstecken',
	
    base_plus_menu_check_mods = 'Überprüfe Kompatibilität (warte darauf)',
    base_plus_menu_check_mods_desc = 'Überprüfe, ob Mods aus Kategorieordner funktionieren könnnen',
	
    base_plus_menu_actions_bltp_check_mods = 'Überprüfe Kompatibilität',
    base_plus_menu_actions_bltp_check_mods_desc = 'Überprüfe, ob Mods aus Kategorieordner funktionieren könnnen'

}